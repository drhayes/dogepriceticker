FROM python:3

WORKDIR /usr/src/app

ADD doge.py /usr/src/app

RUN pip install requests

CMD ["python", "./doge.py"]
