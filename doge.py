#!/usr/bin/env python

import requests

balance_response = requests.get('https://dogechain.info/chain/Dogecoin/q/addressbalance/D7hgEAoMpGterPUiDek76qqshCGM3bVtq4')
balance = float(balance_response.text)

ticker_response = requests.get('https://api.coinmarketcap.com/v1/ticker/dogecoin/')
tickers = ticker_response.json()
doge_to_usd_str = tickers[0]['price_usd']
doge_to_usd = float(doge_to_usd_str)

dollars = balance * doge_to_usd

dollars_str = '${:,.2f}'.format(dollars)
